package sql;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import object.Person;

import java.sql.*;

/**
 * Created by user on 22.03.2016.
 */

public class SQLmanager {
    private ObservableList<Person> _personList = FXCollections.observableArrayList();
    private Connection connection = null;
    private ResultSet rs = null;
    private Statement statement = null;

    /**
     * коннектимся к базе данных
     */
    public void connect(){
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:C:/db/users.db");
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * закрываем соединение с БД
     */
    public void disconnect(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * запрашиваем всех пользователей и формируем PersonList
     * вызывается после connect()
     */
    public void requestAllUsers(){
        try {
            _personList.removeAll();
            rs = statement.executeQuery("select * from main");
            while(rs.next())
            {
                Person p = new Person(rs.getString("name"),rs.getString("surname"));
                _personList.add(p);
//                System.out.print("name = " + rs.getString("name"));
//                System.out.println(" surname = " + rs.getString("surname"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * удаляет из базы данных выбранного человека
     * @param person человек для удаления
     */
    public void delete(Person person){
        try {
            StringBuilder sqlCommand = new StringBuilder();
            //формируем комманду для SQL
            sqlCommand.append("DELETE FROM main WHERE name='").append(person.get_name()).append("' AND surname='").append(person.get_surname()).append("'");
            System.out.println(sqlCommand.toString());
            statement.executeUpdate(sqlCommand.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ObservableList<Person> getPersonList() {
        return _personList;
    }
}
