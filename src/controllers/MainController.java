package controllers;

import collections.UserCollection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import object.Person;
import sql.SQLmanager;

public class MainController {
    public TableView dataTable;
    public TableColumn columnName;
    public TableColumn columnSurname;
    public MenuItem deleteItem;

    private SQLmanager sm;

    @FXML
    private void initialize(){
        columnName.setCellValueFactory(new PropertyValueFactory<Person,String>("_name"));
        columnSurname.setCellValueFactory(new PropertyValueFactory<Person,String>("_surname"));
    }

    public void delete(ActionEvent actionEvent) {
        Person per =(Person) dataTable.getSelectionModel().getSelectedItem();
        if (per != null) {
            sm = new SQLmanager();
            sm.connect();
            sm.delete(per);
            sm.disconnect();
        }
        updateTable(actionEvent);
    }

    public void updateTable(ActionEvent actionEvent) {
        sm = new SQLmanager();
        sm.connect();
        sm.requestAllUsers();
        sm.disconnect();
        dataTable.setItems(sm.getPersonList());
    }

    public void closeProgramm(ActionEvent actionEvent) {
        System.exit(0);
    }
}
