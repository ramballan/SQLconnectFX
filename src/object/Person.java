package object;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by user on 20.03.2016.
 */
public class Person {
    private SimpleStringProperty _name = new SimpleStringProperty("");
    private SimpleStringProperty _surname = new SimpleStringProperty("");

    public Person(String name, String surname) {
        this._name.set(name);
        this._surname.set(surname);
    }

    public String get_name() {
        return _name.get();
    }

    public SimpleStringProperty _nameProperty() {
        return _name;
    }

    public String get_surname() {
        return _surname.get();
    }

    public SimpleStringProperty _surnameProperty() {
        return _surname;
    }
}
