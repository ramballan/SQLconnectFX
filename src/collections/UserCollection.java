package collections;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import object.Person;

/**
 * Created by user on 20.03.2016.
 */
public class UserCollection {
    private ObservableList<Person> _personList = FXCollections.observableArrayList();

    public ObservableList<Person> getPersonlist() {
        return _personList;
    }

    public void add(Person p){ _personList.add(p);}
    public void delete(Person p){ _personList.remove(p);}
    public void fillTestData(){
        add(new Person("Петя","Иванов"));
        add(new Person("Петsdffsя2","jhgj"));
        add(new Person("Петsdffsя2","Ивdsgsdgfанов2"));
        add(new Person("Петsdffsя2","Ивdsgsdgfанов2"));

    }
    public void printData(){
        for(Person per : _personList){
            System.out.println("name = " + per.get_name() + "  number = " + per.get_surname());
        }
    }


}
